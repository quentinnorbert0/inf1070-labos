
## Préparation

**Gâteau**

- Placer la grille au centre du four. Préchauffer le four à 180 °C (350 °F). Beurrer les parois et tapisser deux moules à charnière de 20 cm (8 po) de diamètre de papier parchemin.
- Dans un bol, mélanger la farine, le bicarbonate, la poudre à pâte et le sel. Réserver.
- Dans un autre bol, mélanger les oeufs, le sucre, le lait de beurre, l’huile et la vanille au fouet. Réserver.
- Dans un petit bol, mélanger le cacao et le café au fouet. Ajouter au mélange d’oeufs. Incorporer les ingrédients secs et mélanger jusqu’à ce que la pâte soit homogène. Répartir la pâte dans les moules.
- Cuire au four de 45 à 50 minutes ou jusqu’à ce qu’un cure-dent inséré au centre du gâteau en ressorte propre. Laisser refroidir complètement sur une grille. Démouler.

**Ganache**

- Placer le chocolat dans un bol.
- Dans une petite casserole, porter à ébullition la crème et le sirop de maïs. Verser sur le chocolat. Laisser reposer 2 minutes avant de remuer.
- Mélanger au fouet jusqu’à ce que la ganache soit lisse et homogène. Réfrigérer 1 heure ou jusqu’à ce qu’elle ait épaissi et qu’elle soit manipulable, en la remuant à quatre ou cinq reprises.

**Montage**

- Couper et retirer la calotte des gâteaux pour les rendre plats. Couper les gâteaux en deux à l'horizontale pour obtenir quatre tranches. Répartir la ganache sur les tranches de gâteau et bien l’étaler sur toute la surface à l’aide d’une spatule. Superposer les quatre tranches et déposer sur une assiette de service.

<!-- vim: set sts=4 ts=4 sw=4 tw=80 et :-->

